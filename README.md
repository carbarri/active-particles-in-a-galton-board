# Active Particles in a Galton Board

## Simulations
### Description
The `scripts_lammps` folder contains 6 LAMMPS input scripts grouped in 3 categories:

1. Non-interacting passive particles following either Langevin (`1a.2D-LD.lammps`) or Brownian Dynamics (`1b.2D-BD.lammps`). Non-interacting particles are initialized with almost the same initial positions and are allowed to evolve with free boundary conditions.

2. Non-interacting active particles in the overdamped regime (Brownian dynamics) following either active Brownian dynamics (`2a.2D-ABP.lammps`) or Run-and-Tumble dynamics (`2b.2D-RTP.lammps`). Particles are initialized with almost the same initial positions and are allowed to evolve with free boundary conditions.

3. A galton board is placed composed of fixed particles in an hexagonal lattice. Active non-interacting particles in the overdamped regime (Brownian dynamics) following either active Brownian dynamics (`3a.2D-ABP_GB.lammps`) or Run-and-Tumble dynamics (`3b.2D-RTP_GB.lammps`) are initialized all in the same spot at the top center of the galton board. A gravity force is also implemented allowing particles to flow through the galton board.

### Running the simulations
In order to run the simulations, the lammps executable has to be built with the packages BROWNIAN and DIPOLE. Refer to the [LAMMPS build instructions](https://docs.lammps.org/Build.html) on how to do this, and to the [LAMMPS run instructions](https://docs.lammps.org/Run_basics.html) for running.


## Analysis
The `scripts_analysis` folder contains python notebooks for analyzing the data generated from the input scripts.

- `0.analysis_loading-and-visualization.ipynb`
    Basic loading and visualization tutorial
- `1.analysis_msd.ipynb`
    Simple visualization of the Mean Squared Displacement generated on-the-fly during the lammps bulk simulations (cases 1 and 2).
- `2.analysis_dens-dist_bulk.ipynb`
    Density histograms and kurtosis for the bulk simulations (cases 1 and 2).
- `3.analysis_dens-dist_galton-board.ipynb`
    Density histograms and kurtosis for the Galton board simulations (case 3).
- `definitions.py`
    Python classes and functions to load the data.

## 
![](https://gitlab.com/carbarri/active-particles-in-a-galton-board/-/raw/main/img/GB_horiz-act_bimodal-dist.gif?ref_type=heads)





