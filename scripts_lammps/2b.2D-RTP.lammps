# Run-and-Tumble particles in 2 dimensions (with 'fix brownian')


########
# INIT #
########

	#######################
	# SIMULATION PARAMETERS

	variable    time_step       	equal       0.01
	variable    run_time        	equal       10000000
	variable    sample          	equal       10000
	# variable    conf_sample     	equal       10000
	variable 	N_log_conf_samples  equal 		2000
	variable    rand_vel        	equal       32442

	variable 	alpha 			equal 		0.001
	variable 	Fp 				equal		1.0
	variable 	N				equal		20000
	variable    T 	     	    equal       0.000000001
	variable    Tr 	     	    equal       0.000000001	
	variable	gamma_t        	equal       1.0
	variable	gamma_r        	equal       3.0

	variable	sim_name 		string		ts${time_step}_rt${run_time}_s${sample}_N${N}_T${T}_Tr${Tr}_gt${gamma_t}_gr${gamma_r}_Fp${Fp}_tr${alpha}

	####################
	# SIMULATION FOLDERS

	shell       mkdir	./${sim_name}
	shell		cd 		./${sim_name}
	shell		mkdir 	./Frames

	####################

	units       lj
	dimension   2  
	atom_style  hybrid dipole sphere
    boundary    s s p


##############
# SYSTEM DEF #
##############

	# LAS REGIONES TIENEN QUE IR DESPUES DE ESTO!!

	region box		block -1.0 1.0 -1.0 1.0 0 0.5
	region spawn	sphere  0.0 0.0 0.25 0.0001

	create_box 	1 box
	create_atoms 1 random ${N} 45645 spawn	
	mass * 1.0
	velocity all create 0.0 ${rand_vel} mom yes rot yes dist gaussian
	neigh_modify exclude type 1 1


################
# SIM SETTINGS #
################

	timestep ${time_step}

	compute MSD all msd	

	# Run and Tumble dynamics
	variable 	rndAngle	atom 	2*random(0,3.14159265359,232434)
	fix 		aAngle 		all 	property/atom 	d_aAngle
	set 		atom *		d_aAngle	v_rndAngle
	compute 	aAngleC		all		property/atom 	d_aAngle
	variable 	isTumbling	atom 	(${alpha}>=random(0,1,2378))
	group		tumbling 	dynamic all var isTumbling every 1
	variable 	aForceX	atom 	${Fp}*cos(c_aAngleC)
	variable 	aForceY	atom 	${Fp}*sin(c_aAngleC)
	fix		1 all addforce v_aForceX v_aForceY 0.0 every 1

	# Brownian dynamics
	fix 	2 all brownian/sphere ${T} 45624626 gamma_t ${gamma_t} gamma_r ${gamma_r} rotation_temp ${Tr} rng gaussian
	fix		3 all enforce2d		# last fix always!


	thermo_style  custom etotal ke pe enthalpy temp press pxx
	thermo_modify line multi flush yes
	thermo        ${sample}

	variable Etot 	equal etotal
	variable Ekin 	equal ke
	variable Epot	equal pe
	variable Enth 	equal enthalpy
	variable Temp 	equal temp
	variable Pres 	equal press
	variable Pxx 	equal pxx

	fix 4 all ave/time 10 1 10 c_MSD[*] file ./msd.dat mode scalar
	fix 5 all ave/time 10 10 100 v_Etot v_Ekin v_Epot v_Enth v_Temp v_Pres v_Pxx file ./vars.dat

	variable s equal logfreq3(1,${N_log_conf_samples},${run_time})

	dump          conf_run all custom 1 ./Frames/frame.* id type x y vx vy fx fy
	dump_modify   conf_run sort id every v_s first yes

#######
# RUN #
#######

	# Run simulation
	run ${run_time} pre no post no every 1 "set group tumbling d_aAngle v_rndAngle"
	

