import numpy as np
import sys
import os
import glob, re
import pandas as pd
import matplotlib as mpl
import natsort
from IPython.display import clear_output

def natural_sort_key(s, _nsre=re.compile('([0-9]+)')):
    return [int(text) if text.isdigit() else text.lower()
            for text in _nsre.split(s)] 

def importFiles(filename_glob_pattern, dtype='float', delimiter=None, skip_header=0, usecols=None):
    
    '''Import LAMMPS output dump files.
        
        Example: 
        
        frames = importFiles("/lammps/data/sim01/frames/ConRun.*", dtype=float, delimiter=" ", skip_header=9, usecols=range(0,8))'''
    
    sorted_file_list = sorted(glob.glob(filename_glob_pattern), key=natural_sort_key)
    
    data = []
    i=1;
    for file_path in sorted_file_list:
        
        data.append(
            np.genfromtxt(file_path, delimiter=delimiter, skip_header=skip_header, dtype=dtype, usecols=usecols))
        i+=1;
        if (i % 1000) == 0:
            clear_output(wait=True)
            print(i)
            
    if i==2:
        data = data[0]
    else:
        data = np.array(data)
        
    return np.array(data)



def importFiles_lammps(filename_glob_pattern, dtype='float', delimiter=None, skip_header=9, usecols=None):
    
    '''Import LAMMPS output dump files.
        
        Example: 
        
        frames = importFiles("/lammps/data/sim01/frames/ConRun.*", dtype=float, delimiter=" ", skip_header=9, usecols=range(0,8))'''
    
    sorted_file_list = sorted(glob.glob(filename_glob_pattern), key=natural_sort_key)
    
    data = []
    i=1;
    for file_path in sorted_file_list:
        
        data.append(
            np.genfromtxt(file_path, delimiter=delimiter, skip_header=skip_header, dtype=dtype, usecols=usecols))
        i+=1;
        if (i % 1000) == 0:
            clear_output(wait=True)
            print(i)
            
    if i==2:
        data = data[0]
    else:
        data = np.array(data)
        
    return np.array(data)


def colorFader(c1,c2,mix=0): #fade (linear interpolate) from color c1 (at mix=0) to c2 (mix=1)
    c1=np.array(mpl.colors.to_rgb(c1))
    c2=np.array(mpl.colors.to_rgb(c2))
    return mpl.colors.to_hex((1-mix)*c1 + mix*c2)

############################################################################################
############################################################################################
############################################################################################


class trajectory: 

    def __init__(self):
        ### Simulation step, Number of particles, Number of molecules
        self.step=0
        self.NumPart=0
        self.NumMole=0
        ### Box
        self.boxX=[0.0,0.0]
        self.boxY=[0.0,0.0]
        self.boxZ=[0.0,0.0]
        self.Lx=0.0
        self.Ly=0.0
        self.Lz=0.0
        ### Trajectory
        self.traj = pd.DataFrame({})
        ### For the linked cell list
        self.Ncells=0       ### Number of cells in linked cell list
        self.lscl=[]
        self.head=[]
        self.fields=[]
        self.header=''
        self.bound_cond=''
        
############################################################################################
###### Read frame in lammps format
############################################################################################        
    def read_frame(self, file, file_type='lammps_dump', ignore_first_rows=0):

        ### Read head of the file
        f=open(file)
        
        if file_type == 'lammps_dump':
            
            ### Read head of the file
            self.header += f.readline()

            line = f.readline()
            self.header += line
            partL = line.split()
            self.step=int(partL[0])

            self.header += f.readline()

            line = f.readline()
            self.header += line            
            partL = line.split()
            self.NumPart=int(partL[0])

            line = f.readline()            
            self.header += line
            partL = line.split()
            self.bound_cond = partL[-3:]

            line = f.readline()
            self.header += line                        
            partL = line.split()
            self.boxX=[float(partL[0]),float(partL[1])]
            self.Lx=float(partL[1])-float(partL[0])

            line = f.readline()
            self.header += line                        
            partL = line.split()
            self.boxY=[float(partL[0]),float(partL[1])]
            self.Ly=float(partL[1])-float(partL[0])

            line = f.readline()
            self.header += line                        
            partL = line.split()
            self.boxZ=[float(partL[0]),float(partL[1])]
            self.Lz=float(partL[1])-float(partL[0])

            line=f.readline()
            self.fields = line.split()[2:]

            try:
                self.traj = pd.read_table(file, skiprows = 9, header = None, sep=r"\s+")
                self.traj.columns = self.fields
                
            except pd.errors.EmptyDataError:
                self.traj = pd.DataFrame()
                


            ##############################################################
            ##### Transform scaled coordinates to usual coordinates
            ##############################################################
            if 'xs' in self.fields:
                x=self.traj['xs'].values  #*self.Lx
                for ii in range(0,len(x)):
                    while x[ii] > 1.0:
                        x[ii]=x[ii]-1.0
                    while x[ii] < 0.0: 
                        x[ii]=x[ii]+1.0
                self.traj['xs']=x*self.Lx
                self.traj.columns = self.traj.columns.str.replace('xs', 'x')

            if 'ys' in self.fields:
                y=self.traj['ys'].values  #*self.Ly
                for ii in range(0,len(y)):
                    while y[ii] > 1.0:
                        y[ii]=y[ii]-1.0
                    while y[ii] < 0.0: 
                        y[ii]=y[ii]+1.0
                self.traj['ys']=y*self.Ly
                self.traj.columns = self.traj.columns.str.replace('ys', 'y')   

            if 'zs' in self.fields:
                z=self.traj['zs'].values  #*self.Lz
                for ii in range(0,len(z)):
                    while z[ii] > 1.0:
                        z[ii]=z[ii]-1.0
                    while z[ii] < 0.0: 
                        z[ii]=z[ii]+1.0
                self.traj['zs']=z*self.Lz
                self.traj.columns = self.traj.columns.str.replace('zs', 'z')          

            ##############################################################
            ##### Transform unwrapped coordinates to usual coordinates
            ##############################################################  
            if 'xu' in self.fields:
                x=self.traj['xu'].values  #*self.Lx
                for ii in range(0,len(x)):
                    while x[ii] > self.boxX[1]:
                        x[ii]=x[ii]-self.Lx
                    while x[ii] < self.boxX[0]: 
                        x[ii]=x[ii]+self.Lx
                self.traj['xu']=x
                self.traj.columns = self.traj.columns.str.replace('xu', 'x')

            if 'yu' in self.fields:
                y=self.traj['yu'].values  #*self.Ly
                for ii in range(0,len(y)):
                    while y[ii] > self.boxY[1]:
                        y[ii]=y[ii]-self.Ly
                    while y[ii] < self.boxY[0]: 
                        y[ii]=y[ii]+self.Ly
                self.traj['yu']=y
                self.traj.columns = self.traj.columns.str.replace('yu', 'y')   

            if 'zu' in self.fields:
                z=self.traj['zu'].values  #*self.Lz
                for ii in range(0,len(z)):
                    while z[ii] > self.boxZ[1]:
                        z[ii]=z[ii]-self.Lz
                    while z[ii] < self.boxZ[0]: 
                        z[ii]=z[ii]+self.Lz
                self.traj['zu']=z
                self.traj.columns = self.traj.columns.str.replace('zu', 'z')         
            
        if file_type == 'normal':
            
            # Ignore first rows
            if ignore_first_rows != 0:
                [f.readline() for i in range(0, ignore_first_rows)]             

            line=f.readline()
            self.fields=line.split()[1:]

            self.traj = pd.read_table(file, skiprows = ignore_first_rows+1, header = None, delim_whitespace=True)
            self.traj.columns = self.fields
            
############################################################################################
###### Write frame in lammps format
############################################################################################ 
    def write_frame(self):
        
        new_frame = f'ITEM: TIMESTEP\n{self.step}\n'
        new_frame += f'ITEM: NUMBER OF ATOMS\n{self.NumPart}\n'
        new_frame += f'ITEM: BOX BOUNDS {self.bound_cond[0]} {self.bound_cond[1]} {self.bound_cond[2]}\n'
        new_frame += f'{self.boxX[0]} {self.boxX[1]}\n'
        new_frame += f'{self.boxY[0]} {self.boxY[1]}\n'
        new_frame += f'{self.boxZ[0]} {self.boxZ [1]}\n'
        new_frame += 'ITEM: ATOMS '        
        new_frame += ' '.join(self.fields)

        new_frame += '\n'

        new_frame += '\n'.join([' '.join((row.astype(str))) for row in self.traj.values])

        return new_frame  
        
############################################################################################
######## Compute neighbours using linked cell list algorithm
############################################################################################
    def init_linked_list(self,cutoff):

        Lx=self.Lx
        Ly=self.Ly
        Lz=self.Lz
        NumPart=self.NumPart
        
        ### Number of cells in each direction
        Lca=[int(Lx/cutoff), int(Ly/cutoff), int(Lz/cutoff)]
        rca=[Lx/Lca[0], Ly/Lca[1], Lz/Lca[2]]
        self.Ncells=Lca[0]*Lca[1]*Lca[2]
        
        print("Number of cells:")
        print(Lca, self.Ncells)

        lcyz = Lca[1]*Lca[2]
        lcxyz = lcyz*Lca[0]

        self.lscl=np.zeros(NumPart)        
        self.head=-np.ones(self.Ncells)

        x=self.traj['x'].values
        y=self.traj['y'].values
        z=self.traj['z'].values
        
        ### Compute cell of each atom
        for ii in range(0,NumPart):
            mcx=int(x[ii]/rca[0])
            mcy=int(y[ii]/rca[1])
            mcz=int(z[ii]/rca[2])

            c = int(mcx*lcyz+mcy*Lca[2]+mcz)

            #print(mcx, mcy, mcz, c)
            #print(ii+1,x[ii],y[ii],z[ii],c,self.Ncells)

            if(c>=self.Ncells):
                print('ERROR 001! Number of cells overflow')

            self.lscl[ii]= self.head[c]
            self.head[c] = ii


    def compute_neigh(self,cutoff):

        self.init_linked_list(cutoff)
        
        Lx=self.Lx
        Ly=self.Ly
        Lz=self.Lz

        NPart=self.NumPart

        ### Number of cells in each direction
        Lca=[int(Lx/cutoff), int(Ly/cutoff), int(Lz/cutoff)]
        rca=[Lx/Lca[0], Ly/Lca[1], Lz/Lca[2]]
        self.Ncells=Lca[0]*Lca[1]*Lca[2]

        lcyz = Lca[1]*Lca[2]
        lcxyz = lcyz*Lca[0]

        head=self.head
        lscl=self.lscl

        x=self.traj['x'].values
        y=self.traj['y'].values
        z=self.traj['z'].values

        neighList=[]        

        for mcx in range(0,Lca[0]):
            for mcy in range(0,Lca[1]):
                for mcz in range(0,Lca[2]):

                    #print(mcx,mcy,mcz)

                    ### Cell for this particle
                    c = mcx*lcyz+mcy*Lca[2]+mcz
                    for mc1x in range(mcx-1,mcx+2):
                        for mc1y in range(mcy-1,mcy+2):
                            for mc1z in range(mcz-1,mcz+2):
                                rshift=[0.0, 0.0, 0.0]
                                ## Periodic Boundary Conditions
                                if (mc1x < 0):
                                    rshift[0] = -Lx
                                elif (mc1x>=Lca[0]):
                                    rshift[0] = Lx

                                if (mc1y < 0):
                                    rshift[1] = -Ly
                                elif (mc1y>=Lca[1]):
                                    rshift[1] = Ly

                                if (mc1z < 0):
                                    rshift[2] = -Lz
                                elif (mc1z>=Lca[2]):
                                    rshift[2] = Lz

                                ### Cell for the neigh
                                c1 = ((mc1x+Lca[0])%Lca[0])*lcyz+((mc1y+Lca[1])%Lca[1])*Lca[2]+((mc1z+Lca[2])%Lca[2])

                                i = int(head[c])
                                while (i != -1):
                                    ## Scan atom j in cell c1
                                    j = int(head[c1])
                                    while (j != -1):
                                        ### Different molecule
                                        if (i < j):
                                            ### Compute distance
                                            dx= x[i]-(x[j]+rshift[0])
                                            dy= y[i]-(y[j]+rshift[1])
                                            dz= z[i]-(z[j]+rshift[2])
                                            rr = dx*dx+dy*dy+dz*dz

                                            if (rr < cutoff*cutoff):
                                                neighList.append((i,j))

                                        j = int(lscl[j])

                                    i = int(lscl[i])

        return neighList

    
############################################################################################
######## Calcula la distribución de probabilidad de encontrar una partícula de tipo "tipo" en
######## algún punto a lo largo del eje x. Si tipo='all' calcula la distribucion de todas las
######## particulas sin importar el tipo.
############################################################################################   
def compute_distribution_num_of_particles(x,t,tipo,boxX,normaliza,nbin):
    
    distribucion=np.zeros(nbin)
    intervalos=np.linspace(boxX[0],boxX[1],nbin)
    Lx=boxX[1]-boxX[0]
    binSize=Lx/nbin
    
    if(tipo == 'all'):
        for ii in range(0,len(x)):
            numBin=int(x[ii]/binSize)
            distribucion[numBin]+=1        
    else:
        for ii in range(0,len(x)):
            if(t[ii]==tipo):
                numBin=int(x[ii]/binSize)
                distribucion[numBin]+=1    
    

    if(normaliza):
        if all(item == 0.0 for item in distribucion):
            print('¡ERROR! No hay particulas de tipo '+str(tipo)+', la distribucion resultante es nula')
        else:
            area=0.0
            for ii in range(0,nbin):
                area+=distribucion[ii]*binSize
            distribucion=[ pp/area for pp in distribucion]
    else:
        print('No se normalizara la distribucion')
    
    return intervalos, distribucion





def get_paths(par_path, dir_contains, file_contains=''):
    ''' 
    Returns a list of paths of all files in par_path (and subdirectories) containing dir_contains in their path and containing file_contains in their name. Or if file_contains is not specified, returns list of paths of the directories in par_path that contain dir_contains.
    '''
    file_paths = []
    dir_paths = []
    
    if file_contains == '':
        dir_contains_arr = [dir_contains] if not isinstance(dir_contains, list) else dir_contains  
        for path in os.listdir(par_path):
            for dir_contains in dir_contains_arr:
                if os.path.isdir(os.path.join(par_path, path)) and dir_contains in path:
                    dir_paths.append(os.path.join(par_path, path))

        return natsort.natsorted(dir_paths)
                
    else:
        file_contains = [file_contains] if not isinstance(file_contains, list) else file_contains        
        for root, dirs, files in os.walk(par_path, followlinks=False):
            for file in files:
                path = os.path.join(root, file)
                if all([word in path for word in file_contains]):
                    file_paths.append(os.path.normpath(path))
                    
        return natsort.natsorted(file_paths)

    
    
def ceiling(a, precision=0):
    return np.round(a + 0.5 * 10**(-precision), precision)    
    
def floor(a, precision=0):
    return np.round(a - 0.5 * 10**(-precision), precision)
    
def count_zeroes(number):
    return int(np.abs(np.ceil(np.log10(np.abs(number)))))+1    
    
def get_value_from_path(path, value_name, delimiter='_'):
    start = path.find(value_name)
    end = path.find(delimiter, start)
    return path[start:end] 
    
    
    
def load_frames(sel_files):

    frames = []
    # Si sel_files es una lista o un array de numpy
    if isinstance(sel_files, np.ndarray) or isinstance(sel_files, list):
        # Recorremos todos los frames en la lista dada    
        for i, file in enumerate(sel_files):
    
            # Read file to fill frame variable with values
    
            frame = trajectory()
            frame.read_frame(file, file_type='lammps_dump', ignore_first_rows=0)
            frames.append(frame)    
    
            # if (i % round(len(files)/10)) == 0:
            clear_output(wait=True)
            print(f"Importing files: {i+1} / {len(sel_files)}")
            
    # Si sel_files es una string
    elif isinstance(sel_files, str):
        frame = trajectory()
        frame.read_frame(sel_files, file_type='lammps_dump', ignore_first_rows=0)
        frames.append(frame)

    else:
        raise TypeError("Only strings, lists or numpy arrays are allowed.")
        
    return frames
    

    
def periodic_dist(arr1,arr2,box):
    dist=[0,0,0]
    for i in range(3):
        dist[i] = arr1[i]-arr2[i]
        if ((arr1[i]-arr2[i]) < -box[i]/2):
            dist[i] += box[i]
        if ((arr1[i]-arr2[i]) > box[i]/2):
            dist[i] -= box[i]

    return dist
